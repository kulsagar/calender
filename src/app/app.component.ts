import { Component } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  dayNames = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

  config: Object = {
    autoplay: 3000, // Autoplay option having value in milliseconds
    initialSlide: 3, // Slide Index Starting from 0
    slidesPerView: 3, // Slides Visible in Single View Default is 1
    pagination: '.swiper-pagination', // Pagination Class defined
    paginationClickable: true, // Making pagination dots clicable
    nextButton: '.swiper-button-next', // Class for next button
    prevButton: '.swiper-button-prev', // Class for prev button
    spaceBetween: 10 // Space between each Item
  };
  constructor(private httpCLient: HttpClient) {
  }

  ngOnInit() {    
    const currentYear = new Date().getFullYear();
    this.buildYearCalendar(currentYear);
  }	

   loadCalender() {
               
    this.httpCLient.get('http://localhost:31338/calender_details.json')
      .subscribe(res =>  { 
		localStorage.setItem('importance', JSON.stringify(res));		
	});
  }

  getImportance(selectedDate) {
    this.loadCalender();
    // @ts-ignore
    let json = localStorage.getItem('importance');
    if (json === undefined || json === null) {
      this.loadCalender();
    }
    // @ts-ignore
    json = JSON.parse(localStorage.getItem('importance'));
    return json[selectedDate];
  }

  getDaysInMonth(month, year) {
    const date = new Date(year, month, 1);
    const days = [];
    while (date.getMonth() === month) {
      days.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return days;
  }

  getMonthsInYear(year) {
    const date = new Date(year, 0, 1);
    const months = [];
    let monthCount = 0;
    while (monthCount < 12) {
      months.push(new Date(date));
      date.setMonth(date.getMonth() + 1);
      monthCount++;
    }
    return months;
  }

  getMonthsInRange(startDate, endDate) {
    const start = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
    const end = new Date(endDate.getFullYear(), endDate.getMonth(), 1);
    const months = [];
    let monthCount = 0;
    while (start <= end) {
      months.push( new Date(start) );
      start.setMonth(start.getMonth() + 1);
      monthCount++;
    }
    return months;
  }

  buildYearCalendar(year) {
    const months = this.getMonthsInYear(year);

    const opts = {
      showMonth: true,
      showDaysOfWeek: true,
      showYear: true,
      clickHandler(e) {
        const day = e.target.getAttribute('data-date');
        const dayOfmonth = new Date(day).getDate();
        const month = new Date(day).getMonth();
        const cyear = new Date(day).getFullYear();
        const selectedDate = dayOfmonth + '/' + (month + 1) + '/' + (1900 + cyear);
        alert(selectedDate + ' ' + this.getImportance(selectedDate));
      }
    };

    months.forEach((a, b) => {
      const monthNode = this.buildMonth(b, year, opts);      
      const slide = document.getElementById(this.monthNames[b]);
      slide.appendChild(monthNode);
    });
  }

  buildMonthsInRange(el, opts, startDate, limit) {
    const endDate = new Date( new Date().setDate(startDate.getDate() + limit) );
    let months = this.getMonthsInRange(startDate, endDate);

    opts = opts  || {};
    opts.limitDate = endDate || false;
    if (opts.reverse) { months = months.reverse(); }

    months.forEach((a, b) => {
      const month = a.getMonth();
      const year = a.getFullYear();
      const monthNode = this.buildMonth(month, year, opts);
      el.appendChild(monthNode);
    });
  }

  buildMonth(monthNum, year, opts) {
// if (monthNum === undefined || year === undefined) return "something is missing";
    const dtm = new Date(year, monthNum, 1);
    const dtmMonth = dtm.getMonth();
    const prevM = new Date(dtm.setMonth(dtmMonth - 1));
    const nextM = new Date(dtm.setMonth(dtmMonth + 1));
    const daysInMonth = this.getDaysInMonth(monthNum, year);
    const daysPrevMonth = this.getDaysInMonth(prevM.getMonth(), prevM.getFullYear());
    const daysNextMonth = this.getDaysInMonth(nextM.getMonth(), nextM.getFullYear());
    const monthNode = document.createElement('div');
    const titleNode = document.createElement('h4');
    const skipLength = daysInMonth[0].getDay();
    const preLength = daysInMonth.length + skipLength;
    const postLength = () => {
      if (preLength % 7 === 0) {
        return 0;
      } else {
        if (preLength < 35) {
          return 35 - preLength;
        } else {
          return 42 - preLength;
        }
      }
    };

    monthNode.classList.add('month');

// Add a Title to the month
    if (opts.showMonth) {
      titleNode.innerText = this.monthNames[monthNum] + (opts.showYear ? ' ' + year : '');
      monthNode.appendChild(titleNode);
    }


// Add Days of week to the top row
    if (opts.showDaysOfWeek) {
      this.dayNames.forEach((a, b) => {
        const dayNode = document.createElement('div');
        dayNode.classList.add('dow');
        dayNode.innerText = this.dayNames[b];
        // console.log("a=" + monthNum + " b=" + b);
        monthNode.appendChild(dayNode);
      });
    }


// Add blank days to fill in before first day
    for (let i = 0; i < skipLength; i++) {
      const dayNode = document.createElement('div');
      dayNode.classList.add('dummy-day');
      dayNode.innerText = '' + (daysPrevMonth.length - (skipLength - (i + 1)));
      monthNode.appendChild(dayNode);
    }


// Place a day for each day of the month
    daysInMonth.forEach((c, d) => {
      const today = new Date(new Date().setHours(0, 0, 0, 0));
      const dayNode = document.createElement('div');
      dayNode.classList.add('day');
      dayNode.setAttribute('data-date', c);
      dayNode.innerText = '' + (d + 1);
      const dte = c.getDate() + '/' + (c.getMonth() + 1) + '/' + (c.getYear() + 1900);
      const imp = this.getImportance(dte);
      if (imp !== undefined && imp !== null) {
        // console.log("imp=" + imp + " dte=" + dte );
        dayNode.classList.add('importance');
      }
      const dow = new Date(c).getDay();
      const dateParsed = Date.parse(c);
      const todayParsed = Date.parse(today.toDateString());

      if (dateParsed === todayParsed) { dayNode.classList.add('today'); }
      if (dateParsed > todayParsed) { dayNode.classList.add('future'); }
      if (dateParsed < todayParsed) { dayNode.classList.add('past'); }

      if (dow === 0 || dow === 6) { dayNode.classList.add('weekend'); }
      if (opts.onlyCurrent && c < today) { dayNode.classList.add('dummy-day'); }
      if (opts.limitDate) {
        if (c > opts.limitDate) {
          dayNode.classList.add('dummy-day');
        }
      }

      if (opts.filterDayOfWeek) {
        let valid = false;
        for (let i = 0; i < opts.filterDayOfWeek.length; i++) {
          if (c.getDay() === opts.filterDayOfWeek[i]) {
            valid = true;
          }
        }
        if (!valid) {
          dayNode.classList.add('dummy-day');
        }
      }
      if (opts.clickHandler && !dayNode.classList.contains('dummy-day')) {
        function handleEvent(e) {
          e = e || window.event;
          e.preventDefault();
          e.stopPropagation();
          let touches = false;
          if (!touches) {
            touches = true;
            setTimeout(() => {
              touches = false;
            }, 300);
            opts.clickHandler(e);
          }
        }
        dayNode.addEventListener('touchstart', handleEvent);
        dayNode.addEventListener('mousedown', handleEvent);
      }
      monthNode.appendChild(dayNode);
    });

// Add in the dummy filler days to make an even block
    for (let j = 0; j < postLength(); j++) {
      const dayNode = document.createElement('div');
      dayNode.classList.add('dummy-day');
      dayNode.innerText = (j + 1) + '';
      monthNode.appendChild(dayNode);
    }
    return monthNode;
  }


}

